﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiffieHellman
{
	class Program
	{
		static void Main(string[] args)
		{
			var a = DiffieHellman.GenerateRandomBigNumber();
			var p = DiffieHellman.GenerateRandomPrime();
			var result = DiffieHellman.Calculate(a, p);

			Console.WriteLine("a used :" + a);
			Console.WriteLine();
			Console.WriteLine("p used :" + p);
			Console.WriteLine();
			Console.WriteLine("Result:" + result);
		}
	}
}
